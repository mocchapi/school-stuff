#!/usr/bin/python3
import os

def vragen_ophalen(filename='meerkeuzevragen.txt'):
	meerkeuzevragen = {}
	with open(filename,'r') as f:
		raw = f.read()
	for item in raw.split('\n'):
		stuff = item.split(';')
		meerkeuzevragen[stuff[0]] = stuff[1:]
	return meerkeuzevragen


def main(loop=True):
	while loop:
		filename = 'antwoorden_gebruiker.txt'
		print('[q] quit program\n[1] vragenlijst doorlopen\n[2] laatste uitslag tonen')
		puttedin = input('>')
		if puttedin == 'q':
			exit()
		elif puttedin == '1':
			meerkeuzevragen = vragen_ophalen()
			vraag_en_antwoord(meerkeuzevragen,filename=filename)
		elif puttedin == '2':
			if os.path.isfile(filename):
				with open(filename,'r') as f:
					print(f.read())
			else:
				print('geen resultaten')


def vraag_en_antwoord(meerkeuzevragen,filename='antwoorden_gebruiker.txt'):
	antwoorden = []
	for index,key in enumerate(meerkeuzevragen):
		print(f'Vraag {index}: {key}')
		for antwoordindex,item in enumerate(meerkeuzevragen[key]):
			print(f'[{antwoordindex}] {item}')
		antwoorden.append(f'{index} - {meerkeuzevragen[key][int(input(":"))]}')
	with open(filename,'w') as f:
		f.write('\n'.join(antwoorden))

if __name__ == '__main__':
	main()